import React, { Component } from 'react';
import axios from 'axios';
import { Image, Table } from 'react-bootstrap';
import './ShowVisitList.css';


class ShowVisitList extends Component {
  constructor(props) {
    super(props);
    let visits = [];
    this.state = {
      visits: visits
    };
  }

  componentDidMount() {
    let visits = [];
    axios
      .get(process.env.REACT_APP_BACKEND_URL + '/images')
      .then(res => {
        visits = res.data;
        this.setState({visits: visits});
      })
      .catch(err =>{
        console.log('Error from ShowVisitList');
        console.log(err);
      });
  };

  render() {
      const visits = this.state.visits.map((visit, index) =>
      <tr key={index}>
        <td>{visit.timestamp}</td>
        <td>{visit.userIp}</td>
        <td><Image style={{width: '25%'}} src={visit.imageUrl} thumbnail /></td>
        <td>{visit.bestGuess}</td>
        <td>{visit.accuracy ? (visit.accuracy * 100).toFixed(2) + '%': 'Not Available'}</td>
      </tr>);
      return (
        <React.Fragment>
          <div className={"ShowVisitList"}>
            <h2>{this.props.header}</h2>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Timestamp</th>
                  <th>AddrHash</th>
                  <th>Image Link</th>
                  <th>Best Guess</th>
                  <th>Accuracy</th>
                </tr>
              </thead>
              <tbody>
              {visits}
              </tbody>
            </Table>
          </div>
        </React.Fragment>
      );
    }
}

export default ShowVisitList;

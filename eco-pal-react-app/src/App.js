import React, { Component } from 'react';
import ShowVisitList from './ShowVisitList'
import axios from 'axios';
import './App.css';
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <div className={"App"}>
          <h2>Upload an image</h2>
          NOTE: All images uploaded are automatically publically available for
          this project. Do NOT upload an image you wouldn't want your family to see.
          <input className={"Uploader"} type="file" name="file" onChange={this.onChangeHandler}/>
        </div>
        <ShowVisitList header='Last 10 Uploaded Images'/>
      </React.Fragment>
    );
  }

  onChangeHandler=event=>{
    const data = new FormData();
    data.append('image', event.target.files[0]);
    axios
      .post(process.env.REACT_APP_BACKEND_URL + '/upload', data)
      .then(function(response) {
        window.location.reload(false);
      })
      .catch(err =>{
        console.log('Error uploading image');
        console.log(err);
      });
  }
}

export default App;

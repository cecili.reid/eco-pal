# EcoPal

EcoPal is a website that takes images and determines if items in the image can be composted. This repository has two applications: a Node app for the backend which takes in an image, stores it, and processes it with a machine learning model and a React app that shows the 10 latest images uploaded and their predictions from the machine learning model. Below is how to use the application locally and deploy it.

[Link to Devnexus 2020 Slides](https://docs.google.com/presentation/d/1tgBeCbF6X87L6oM9jFFGLKarelHOWIRrLOwcmRWJw9M/edit)

## How to run locally
- Create a GCP account and save the GOOGLE_APPLICATION_CREDENTIALS to a local file.
- Add GOOGLE_APPLICATION_CREDENTIALS to `.env.local` file in the node application folder. i.e. `GOOGLE_APPLICATION_CREDENTIALS=/Users/your_user/google_credentials.json`
- Create a storage bucket and update `.env.local` in the node application folder `BUCKET_NAME=someBucketName`
- Build the node application with `npm i`
- Start the node application with `npm start`
- Use Postman or curl to upload an image to http://localhost:8082/upload. The node application logs will show predictions from the machine learning model
- You could also run the react application locally and visit localhost:3000 to upload an image and see it appear in the table along with accuracy after processing.
  ```cd eco-pal-react-app
  npm i
  npm start```

## Setting up the application for deployment
- Create a GCP account and save the GOOGLE_APPLICATION_CREDENTIALS to a local file.
- Follow the tutorial below to use GCP Firestore in Datastore mode
- Follow tutorial below to get application into App Engine (standard)
- Add GOOGLE_APPLICATION_CREDENTIALS to `.env.local` file in the node application folder. i.e. `GOOGLE_APPLICATION_CREDENTIALS=/Users/your_user/google_credentials.json`
- Create a storage bucket and update `.env.local` in the node application folder `BUCKET_NAME=someBucketName`
- For the node application,
`gcloud app deploy`
- For the react application,
```npm run build
gcloud app deploy```

NOTE: There are some tensorflow dependencies that do not port well over to appengine so the node application using tensorflow.js may not deploy successfully

## What's next?
The next step is to try and build my own model with a dataset more suitable to the images I will have. This may entail building my own dataset or figuring out how to modify an existing dataset to do what I need.

### Google Tensorflow.js
- https://www.tensorflow.org/js/guide
Guides to learn more about tensors and models

### Machine Learning
- Pretrained model and transfer learning: https://codelabs.developers.google.com/codelabs/tensorflowjs-teachablemachine-codelab/index.html#0
- Supervised vs Unsupervised Learning: https://developers.google.com/machine-learning/problem-framing/cases
- https://becominghuman.ai/image-classification-machine-learning-in-node-js-with-tensorflow-js-dd8e20ba5024

### Google GCP
- https://cloud.google.com/appengine/docs/standard/nodejs/quickstart
- https://cloud.google.com/appengine/docs/flexible/nodejs/using-cloud-datastore

### Mern Stack Resources
- https://blog.logrocket.com/mern-stack-a-to-z-part-1/
- https://blog.logrocket.com/mern-stack-a-to-z-part-2/
- https://blog.cloudboost.io/creating-your-first-mern-stack-application-b6604d12e4d3

### Node Resources
- https://nodejs.org/en/docs/guides/debugging-getting-started/

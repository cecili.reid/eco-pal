'use strict';
// import * as mobilenet from '@tensorflow-models/mobilenet';
require('dotenv').config();
const express = require('express');
const crypto = require('crypto');
const cors = require('cors');
const multer = require('multer');

const {Datastore} = require('@google-cloud/datastore');
const {Storage} = require('@google-cloud/storage');

// Import key dependencies for tensorflow.js
// MobileNet is a pretrained model that takes in images: https://www.npmjs.com/package/@tensorflow-models/mobilenet
const mobilenet = require('@tensorflow-models/mobilenet');
// Provides the tensorflow API https://www.npmjs.com/package/@tensorflow/tfjs-node
const tfnode = require('@tensorflow/tfjs-node');
const knnClassifier = require('@tensorflow-models/knn-classifier');
const classifier = knnClassifier.create();
const storageUrl = 'https://storage.googleapis.com';
const bucketName = process.env.BUCKET_NAME;
const JPEG = 'image/jpeg';
const JPG = 'image/jpg';
const PNG = 'image/png';
const VALID_TYPES = [JPEG, JPG, PNG];

// Create instanve of GCP Datastore
const datastore = new Datastore();

// Creates a client
const storage = new Storage();

// const net = new mobilenet.MobileNet();

const upload = multer({storage: multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024, // no larger than 5mb, you can change as needed.
  },
  // Filter file types for PNG/JPG images only
  fileFilter: function(req, file, cb) {
    const msg = 'Invalid mimetype provided. Only the following types are' +
    'allowed: ' + VALID_TYPES.join();
    if (!VALID_TYPES.includes(file.mimetype)) {
      fileError = msg;
      cb(null, false, new Error(msg));
    }
    cb(null, true);
  },
});

const app = express();
app.enable('trust proxy');

// Turn on cors
app.use(cors({origin: true}));

/**
* Analyze the image and store it.
* @param {object} image
* @param {string} ip
*/
async function analyzeImage(image, ip) {
  console.log('Loading mobilenet..');
  // Load the pretrained mobileNet model
  const loadedNet = await mobilenet.load();
  console.log('Successfully loaded model');
  // Translate the image into a format that the mobileNet model can handle using
  // the tensorflowjs API
  const tfimage = tfnode.node.decodeImage(image.buffer);
  // Wait for the model to classify the image and store the result
  const result = await loadedNet.classify(tfimage);
  console.log(result);
  // Find the top performing result. Result is already ordered from most to
  // least probablity so just take the first value.
  const topResult = result.find((item) => item.probability);
  // Take the best result's best guess for the image
  const bestGuess = topResult.className;
  const accuracy = topResult.probability;
  // The public URL can be used to directly access the file via HTTP.
  const imageUrl = storageUrl + '/' + bucketName + '/' + image.originalname;

  const imageData = {
    timestamp: new Date(),
    // Store a hash of the visitor's ip address
    userIp: crypto
        .createHash('sha256')
        .update(ip)
        .digest('hex')
        .substr(0, 7),
    imageUrl: imageUrl,
    bestGuess: bestGuess,
    accuracy: accuracy,
  };

  insertImageRecord(imageData);
  tfimage.dispose();
  return result;
}

/**
* Use the provided classifier to teach the model to start recognizing the image;
* @param {object} image
* @param {string} name
*/
async function teachModel(image, name) {
  console.log('Loading mobilenet..');
  // Load the pretrained mobileNet model
  const loadedNet = await mobilenet.load();
  console.log('Successfully loaded model');
  // Get the intermediate activation of MobileNet 'conv_preds' and pass that
  // to the KNN classifier.
  const tfimage = tfnode.node.decodeImage(image.buffer);
  const activation = loadedNet.infer(tfimage, 'conv_preds');
  // Pass the intermediate activation to the classifier.
  classifier.addExample(activation, name);

  // Get the most likely class and confidences from the classifier module.
  const result = await classifier.predictClass(activation);
  console.log(result);

  // Dispose the tensor to release the memory.
  tfimage.dispose();
}
/**
 * Insert a image record into the database.
 *
 * @param {object} image The image record to insert.
 * @return {object}
 */
const insertImageRecord = (image) => {
  return datastore.save({
    key: datastore.key('image'),
    data: image,
  });
};

/**
 * Retrieve the latest 10 image records from the database.
 * @return {object}
 */
const getImageRecords = () => {
  const query = datastore
      .createQuery('image')
      .order('timestamp', {descending: true})
      .limit(10);

  return datastore.runQuery(query);
};

app.get('/images', async (req, res, next) => {
  try {
    const [entities] = await getImageRecords();
    res.json(entities);
  } catch (error) {
    next(error);
  }
});

// Endpoint to upload an image with a pretrained model
app.post('/upload', upload.single('image'), async (req, res, next) => {
  const file = req.file;
  if (!file) {
    res.status(500);
    return next('No file provided; file required');
  }
  if (req.fileError) {
    res.status(500);
    return next(req.fileError);
  }
  const filename = file.originalname;
  // Create a new blob in the bucket and upload the file data.
  const blob = storage.bucket(bucketName).file(filename);
  const blobStream = blob.createWriteStream({
    resumable: false,
    contentType: file.mimetype,
  });

  blobStream.on('error', (err) => {
    next(err);
  });

  blobStream.on('finish', () => {
    // Create a image record to be stored in the database
    try {
      const name = req.body.classifier;
      if (name) {
        teachModel(file, name).then(analyzeImage(file, req.ip));
      } else {
        analyzeImage(file, req.ip);
      }
    } catch (error) {
      next(error);
    }
    console.log(`${filename} uploaded to ${bucketName}.`);
    res.json({fileName: filename});
  });

  blobStream.end(file.buffer);
});

const PORT = process.env.PORT || 8082;
app.listen(process.env.PORT || 8082, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});
